﻿using BDD.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace BDD.Steps
{
    [Binding]
    public sealed class RozetkaStepDefinitions
    {
        IWebDriver driver;
        HomePage homePage;
        SearchPage searchPage;
        ShoppingCartPage shoppingCartPage;

        [Given(@"Rozetka Homepage (.*)")]
        public void GivenRozetkaHomepage(string url)
        {
            driver = new ChromeDriver(System.AppDomain.CurrentDomain.BaseDirectory + "../../../Driver");
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
        }
        [When(@"Search (.*) with (.*)")]
        public void WhenSearchWith(string product, string name)
        {
            homePage = new HomePage(driver);
            homePage.FindsByText(product);
            searchPage = new SearchPage(driver);
            searchPage.FindsByTextBrand(name);
            searchPage.Rang();
            searchPage.AddToShoppingCartFirstElement();
            searchPage.OpenShoppingCart();
            shoppingCartPage = new ShoppingCartPage(driver);
        }
        [Then(@"Check if the price is lower (.*)")]
        public void ThenCheckIfThePriceIsLower(string price)
        {
            NUnit.Framework.Assert.IsTrue(searchPage.ShoppingCartPrice() > Convert.ToDouble( price)
                || shoppingCartPage.ShoppingCartPrice() > Convert.ToDouble(price));
            driver.Quit();
        }


    }
}
