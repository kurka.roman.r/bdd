﻿Feature: Rozetka

@mytag
Scenario: Rozetka
	Given Rozetka Homepage <url>
	When Search <product> with <name>
	Then Check if the price is lower <priceless>
	
	Examples:
	| product | name    | priceless | url                        |
	| Телефон | Samsung | 20000     | https://rozetka.com.ua/ua/ |